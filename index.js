/**
 * Copyright (c) 2019 Kota Suizu
 * Released under the MIT license
 * http://opensource.org/licenses/mit-license.php
 **/

var util = require("util");
var when = require("when");
var bcrypt = require("bcrypt");

util.log("Node-RED-Starter Starting on IBM Cloud");
util.log("Node-RED-Starter Loading settings.js");
var settings = require("./settings.js");

if (!settings.adminAuth && process.env.NODE_RED_USERNAME && process.env.NODE_RED_PASSWORD) {
    // No user-defined security
    util.log("Node-RED-Starter Enabling adminAuth using NODE_RED_USERNAME/NODE_RED_PASSWORD");
    var config = {
        adminAuth: {
            username: process.env.NODE_RED_USERNAME,
            password: bcrypt.hashSync(process.env.NODE_RED_PASSWORD, 8),
            allowAnonymous: (process.env.NODE_RED_GUEST_ACCESS === 'true')
        }
    };
    startNodeRED(config);
} else {
    util.log("Node-RED-Starter Enabling adminAuth using settings.adminAuth");
    startNodeRED({});
}

function startNodeRED(config) {
    util.log("Node-RED-Starter userDir: " + settings.userDir);
    util.log("Node-RED-Starter flowFile: " + settings.flowFile);
    util.log("Node-RED-Starter nodesDir: " + settings.nodesDir);

    if (config.adminAuth && !settings.adminAuth) {
        util.log("Node-RED-Starter Enabling adminAuth security - set NODE_RED_USERNAME and NODE_RED_PASSWORD to change credentials");
        settings.adminAuth = {
            type: "credentials",
            users: function(username) {
                if (config.adminAuth.username == username) {
                    return when.resolve({
                        username: username,
                        permissions: "read"
                    });
                } else {
                    return when.resolve(null);
                }
            },
            authenticate: function(username, password) {
                if (config.adminAuth.username === username && bcrypt.compareSync(password, config.adminAuth.password)) {
                    return when.resolve({
                        username: username,
                        permissions: "read"
                    });
                } else {
                    return when.resolve(null);
                }
            }
        };
        if ((process.env.NODE_RED_GUEST_ACCESS === 'true') || (process.env.NODE_RED_GUEST_ACCESS === undefined && config.adminAuth.allowAnonymous)) {
            util.log("Node-RED-Starter Enabling anonymous read-only access - set NODE_RED_GUEST_ACCESS to 'false' to disable");
            settings.adminAuth.default = function() {
                return when.resolve({
                    anonymous: true,
                    permissions: "read"
                });
            };
        } else {
            util.log("Node-RED-Starter Disabled anonymous read-only access - set NODE_RED_GUEST_ACCESS to 'true' to enable");
        }
    }

    if (!config.adminAuth && !settings.adminAuth) {
        settings.disableEditor = true;
    } else {
        if (process.env.NODE_RED_DISABLE_EDITOR) {
            if (process.env.NODE_RED_DISABLE_EDITOR.toLowerCase() === 'true') {
                settings.disableEditor = true;
            } else if (process.env.NODE_RED_DISABLE_EDITOR.toLowerCase() === 'false') {
                settings.disableEditor = false;
            }
        }
    }
    util.log("Node-RED-Starter disableEditor: " + settings.disableEditor);


    var dash;
    // ensure the environment variable overrides the settings
    if ((process.env.NODE_RED_USE_APPMETRICS === 'true') || (settings.useAppmetrics && !(process.env.NODE_RED_USE_APPMETRICS === 'false'))) {
        dash = require('appmetrics-dash');
        dash.attach();
    }
    util.log("Node-RED-Starter useAppmetrics: " + settings.useAppmetrics);

    util.log("Node-RED-Starter Loading application settings done. Starting Node-RED...");
    require('./node_modules/node-red/red.js');
}
